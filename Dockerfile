# syntax=docker/dockerfile:1.0.0-experimental
FROM php:7.4-cli-alpine3.11 AS base
FROM composer:1 AS composer

FROM base as source

RUN apk add --no-cache git

COPY --from=composer /usr/bin/composer /usr/bin/composer

WORKDIR /opt/archived

# Mount the current directory at `/opt/project` and run git archive
# hadolint ignore=SC2215
RUN --mount=type=bind,source=./,rw \
    mkdir -p /opt/project \
    && git archive --verbose --format tar HEAD | tar -x -C /opt/project

WORKDIR /opt/project

RUN composer global require hirak/prestissimo

# Mount composer.auth to the project root and composer cache if available
# then install the dependencies
# hadolint ignore=SC2215
#--mount=type=secret,id=composer.auth,target=/opt/project/auth.json \
 #    --mount=type=bind,source=.composer/cache,target=/opt/.composer/cache \
RUN composer install --no-interaction --no-progress --no-dev --prefer-dist --classmap-authoritative

FROM php:7.4-cli-alpine3.11 as prod

WORKDIR /opt/project

COPY --chown=www-data:www-data --from=source /opt/project /opt/project

ENTRYPOINT ["/opt/project/stamphpede"]
