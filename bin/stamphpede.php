<?php

define('CONFIG_PATH_STACK', ['/stamphpede', getcwd(), __DIR__ . '/../',]);

require __DIR__ . '/../vendor/autoload.php';

use Dotenv\Dotenv;
use Stamphpede\Config\ConfigLoader;
use Stamphpede\ServiceManager\ContainerBuilder;

use Stamphpede\Client\Console\GenerateConfig;
use Stamphpede\Client\Console\RunCommand;

use Symfony\Component\Console\Application;
use Symfony\Component\Console\CommandLoader\ContainerCommandLoader;

// Load Env File
//$dotenv = Dotenv::createImmutable(CONFIG_PATH_STACK, '.env');
//$dotenv->load();

//@TODO question: should we load the .env from the working directory instead of the stamphede directory to improve developer workflow?

$containerBuilder = new ContainerBuilder([
    \Stamphpede\Config\Module::class,
    \Stamphpede\Client\Module::class,
]);

$container = $containerBuilder->buildContainer();

$configLoader = $container->get(ConfigLoader::class);
$container->add('config', $configLoader->loadConfig(CONFIG_PATH_STACK, 'client.yaml'));

$commandLoader = new ContainerCommandLoader($container, [
    'run' => RunCommand::class,
    'generate-config' => GenerateConfig::class,
]);

$application = new Application();
$application->setCommandLoader($commandLoader);

$application->run();
