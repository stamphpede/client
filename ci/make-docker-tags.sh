#!/bin/bash

if echo $CI_COMMIT_TAG | grep -Eq '^[0-9]\.[0-9]\.[0-9]$'; then
  MAJOR=$CI_REGISTRY_IMAGE":"`echo $CI_COMMIT_TAG | cut -f1 -d.`
  MINOR=$MAJOR"."`echo $CI_COMMIT_TAG | cut -f2 -d.`
  docker tag $IMAGE_TAG $MAJOR
  docker push $MAJOR
  docker tag $IMAGE_TAG $MINOR
  docker push $MINOR
fi;
