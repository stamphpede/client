<?php

namespace Stamphpede\Client\Console;

use ParagonIE\Sapient\CryptographyKeys\SigningSecretKey;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Tag\TaggedValue;
use Symfony\Component\Yaml\Yaml;

class GenerateConfig extends Command
{
    protected static $defaultName = 'generate-config';

    public function __construct()
    {
        parent::__construct(GenerateConfig::$defaultName);
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Generates keys for secure client/server communication')
            ->addArgument('config-directory', InputArgument::REQUIRED, 'Directory to write config to')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $configDirectory = rtrim($input->getArgument('config-directory'), '/');

        $secretKey = SigningSecretKey::generate();
        $publicKey = $secretKey->getPublicKey();

        $config = [
            'parameters' => [],
            'workdir' => new TaggedValue('env', 'WORKDIR'),
            'test-suites' => [
                'source' => [
                    'type' => 'api',
                    'key' => $publicKey->getString()
                ]
            ]
        ];

        $clientConfig = [
            'key' => $secretKey->getString(),
            'serverUrl' => 'http://stamphpede_server:9000'
        ];

        \file_put_contents($configDirectory . '/config.yaml', Yaml::dump($config));
        \file_put_contents($configDirectory . '/client.yaml', Yaml::dump($clientConfig));

        return 0;
    }
}
