<?php

namespace Stamphpede\Client\Console;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use ParagonIE\ConstantTime\Base64UrlSafe;
use ParagonIE\Sapient\CryptographyKeys\SigningSecretKey;
use ParagonIE\Sapient\Sapient;
use Psr\Http\Message\RequestInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

use Stamphpede\Client\RunnerStats;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use function GuzzleHttp\Psr7\stream_for;


class RunCommand extends Command
{
    protected static $defaultName = 'run';
    private ParameterBag $config;
    private $client;

    public function __construct(ParameterBag $config)
    {
        $this->client = new Client();
        parent::__construct(RunCommand::$defaultName);
        $this->config = $config;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Run a stamphpede test suite')
            ->addArgument('file', InputArgument::REQUIRED, 'file to run');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $fileToRun = $input->getArgument('file');

        if (!file_exists($fileToRun)) {
            $output->writeln('<error>File not found: ' . $fileToRun);
            return 1;
        }

        $logger = new ConsoleLogger($output);

        $request = $this->postRequest(\fopen($fileToRun, "r"), '/test-suite');
        $response = $this->client->send($request);

        $data = \json_decode($response->getBody()->getContents(), \JSON_OBJECT_AS_ARRAY);
        $testSuiteUri = $data['_links']['self']['href'];
        $testSuiteId = $data['id'];

        $output->writeln('Waiting for test suite to be ready...');
        $this->waitFor($testSuiteUri, 'parsing', 15);

        $request = $this->postRequest(\json_encode(['test_suite' => $testSuiteId]), '/run');
        $response = $this->client->send($request);

        $data = \json_decode($response->getBody()->getContents(), \JSON_OBJECT_AS_ARRAY);
        $runUri = $data['_links']['self']['href'];
        $resultsUri = $data['_links']['results']['href'];

        $output->writeln('Waiting for run to finish...');
        $this->waitFor($runUri, 'running', 600);

        $request = $this->getRequest($resultsUri);
        $response = $this->client->send($request);
        $data = \json_decode($response->getBody()->getContents(), \JSON_OBJECT_AS_ARRAY);

        $this->renderStats(new RunnerStats($data['data']), $output);

        return 0;
    }

    protected function postRequest($body, string $uri): RequestInterface
    {
        return $this->request('POST', $body, $uri);
    }

    protected function getRequest(string $uri): RequestInterface
    {
        return $this->request('GET', null, $uri);
    }

    protected function request(string $method, $body, string $uri): RequestInterface
    {
        $signSecret = new SigningSecretKey(Base64UrlSafe::decode($this->config->get('key')));
        $serverUrl = rtrim($this->config->get('serverUrl'), '/');

        $request = new Request($method, $serverUrl . $uri);
        if ($body !== null) {
            $request = $request->withBody(stream_for($body));
        }
        $request = (new Sapient())->signRequest($request, $signSecret);
        return $request;
    }

    private function renderStats(RunnerStats $runnerStats, OutputInterface $output): void
    {
        // Stats
        $table = new Table($output);
        $tableRows = [];
        foreach ($runnerStats->getData() as $taskName => $stat) {
            $responseTimes = isset($stat['responses']['times']) ? $stat['responses']['times'] : [0];
            $tableRows[] = [
                $taskName,
                $stat['method'],
                $stat['url'],
                $stat['numRequests'],
                isset($stat['responses']['numFailures']) ? $stat['responses']['numFailures'] : $stat['numRequests'],
                $runnerStats->getAverageResponseTime($responseTimes),
                $runnerStats->getMinResponseTimeInMilliseconds($responseTimes),
                $runnerStats->getMaxResponseTimeInMilliseconds($responseTimes),
            ];
        }

        $table
            ->setHeaders(['Task', 'Method', 'Url', '# Requests', '# Fails', 'Average (ms)', 'Min (ms)', 'Max (ms)'])
            ->setRows($tableRows);

        $table->render();
    }

    protected function waitFor($testSuiteUri, string $status, int $maxWaitTime): void
    {
        do {
            sleep(1);
            $request = $this->getRequest($testSuiteUri);
            $response = $this->client->send($request);
            $data = \json_decode($response->getBody()->getContents(), \JSON_OBJECT_AS_ARRAY);
            echo '.';
            $maxWaitTime --;
            if ($maxWaitTime < 0) {
                throw new \RuntimeException('waited too long');
            }
        } while ($data['status'] === $status);
        echo "\n";
    }
}
