<?php

namespace Stamphpede\Client;

class Module
{
    public function getServices(): array
    {
        return [
            Console\GenerateConfig::class => [],
            Console\RunCommand::class => [
                'args' => [
                    'config',
                ]
            ]
        ];
    }
}
