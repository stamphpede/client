<?php

namespace Stamphpede\Client;

use Stamphpede\Request;
use Stamphpede\Response;

class RunnerStats
{
    private array $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getAverageResponseTime(array $responseTimes): int
    {
        return $this->convertMicrotimeToMilliseconds(
            array_sum($responseTimes) / count($responseTimes)
        );
    }

    public function getMinResponseTimeInMilliseconds(array $responseTimes): int
    {
        return $this->convertMicrotimeToMilliseconds(
            min($responseTimes)
        );
    }

    public function getMaxResponseTimeInMilliseconds(array $responseTimes): int
    {
        return $this->convertMicrotimeToMilliseconds(
            max($responseTimes)
        );
    }

    private function convertMicrotimeToMilliseconds(float $time): int
    {
        return (int) round($time * 1000);
    }

    public function getData(): array
    {
        return $this->data;
    }
}
